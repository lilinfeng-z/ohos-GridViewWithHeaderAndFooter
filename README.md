# ohos-GridViewWithHeaderAndFooter

## 项目介绍
- 项目名称：ohos-GridViewWithHeaderAndFooter
- 所属系列：OpenHarmony的第三方组件适配移植
- 功能：支持给网格布局的ListContainer添加头布局、尾布局
- 项目移植状态：主功能完成
- 调用差异：无
- 开发版本：sdk5，DevEco Studio2.1 Release
- 基线版本：Releases 1.0.12

## 效果演示
<img src="img/demo.gif"></img>

## 安装教程
1.在项目根目录下的build.gradle文件中，
 ```
allprojects {
    repositories {
        maven {
            url 'https://s01.oss.sonatype.org/content/repositories/snapshots/'
        }
    }
}
 ```

2.在entry模块的build.gradle文件中，
 ```
 dependencies {
    implementation('com.gitee.chinasoft_ohos:GridViewWithHeaderAndFooter:0.0.1-SNAPSHOT')
    ......  
 }

```
在sdk5，DevEco Studio2.1 Release下项目可直接运行
如无法运行，删除项目.gradle,.idea,build,gradle,build.gradle文件，
并依据自己的版本创建新项目，将新项目的对应文件复制到根目录下

## 使用说明
```java
 //获取主布局
 GridViewWithHeaderAndFooter gridViewWithHeaderAndFooter =
                (GridViewWithHeaderAndFooter) findComponentById(ResourceTable.Id_ly_image_list_grid);
 /* 获取头尾布局 */
 LayoutScatter layoutScatter = LayoutScatter.getInstance(context);
 Component headerView = layoutScatter.parse(ResourceTable.Layout_header_item, null, false);
 Component footerView = layoutScatter.parse(ResourceTable.Layout_footer_item, null, false);
 /* 主布局添加头尾布局 */
 gridViewWithHeaderAndFooter.addHeaderView(headerView);
 gridViewWithHeaderAndFooter.addFooterView(footerView);

 gridViewWithHeaderAndFooter.setNumColumns(2);
 gridViewWithHeaderAndFooter.setItemProvider(new GridViewAdapter());       

```

## 测试信息
CodeCheck代码测试无异常

CloudTest代码测试无异常

火绒安全病毒安全检测通过

当前版本demo功能与原组件基本无差异


## 版本迭代
- 0.0.1-SNAPSHOT

## 版权和许可信息
```
 Apache 2
```