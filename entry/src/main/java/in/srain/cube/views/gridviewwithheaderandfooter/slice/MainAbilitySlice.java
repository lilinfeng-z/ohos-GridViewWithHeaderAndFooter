/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package in.srain.cube.views.gridviewwithheaderandfooter.slice;

import in.srain.cube.views.GridViewWithHeaderAndFooter;
import in.srain.cube.views.gridviewwithheaderandfooter.ResourceTable;
import ohos.aafwk.ability.AbilitySlice;
import ohos.aafwk.content.Intent;
import ohos.agp.components.*;
import ohos.agp.window.dialog.ToastDialog;
import ohos.app.Context;

/**
 * MainAbilitySlice
 *
 * @since 2021-04-14
 * Created by huzhenhao on 2021/4/14 10:50
 */
public class MainAbilitySlice extends AbilitySlice {
    public static final int[] mDogs = {ResourceTable.Media_dog0, ResourceTable.Media_dog1, ResourceTable.Media_dog2,
            ResourceTable.Media_dog3, ResourceTable.Media_dog4, ResourceTable.Media_dog5, ResourceTable.Media_dog6,
            ResourceTable.Media_dog7, ResourceTable.Media_dog8, ResourceTable.Media_dog0, ResourceTable.Media_dog1,
            ResourceTable.Media_dog2, ResourceTable.Media_dog3, ResourceTable.Media_dog4, ResourceTable.Media_dog5,
            ResourceTable.Media_dog6, ResourceTable.Media_dog7, ResourceTable.Media_dog8, ResourceTable.Media_dog0};

    private Context context;

    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setUIContent(ResourceTable.Layout_ability_main);
        context = this;
        initLists();
    }

    @Override
    public void onActive() {
        super.onActive();
    }

    @Override
    public void onForeground(Intent intent) {
        super.onForeground(intent);
    }

    private void initLists() {
        GridViewWithHeaderAndFooter gridViewWithHeaderAndFooter =
                (GridViewWithHeaderAndFooter) findComponentById(ResourceTable.Id_ly_image_list_grid);
        /* 获取头尾布局 */
        LayoutScatter layoutScatter = LayoutScatter.getInstance(context);
        Component headerView = layoutScatter.parse(ResourceTable.Layout_header_item,
                null, false);
        Component footerView = layoutScatter.parse(ResourceTable.Layout_footer_item,
                null, false);
        /* 主布局添加头尾布局 */
        gridViewWithHeaderAndFooter.addHeaderView(headerView);
        gridViewWithHeaderAndFooter.addFooterView(footerView);

        gridViewWithHeaderAndFooter.setNumColumns(2);
        gridViewWithHeaderAndFooter.setItemProvider(new GridViewAdapter());

        gridViewWithHeaderAndFooter.setItemClickedListener(new ListContainer.ItemClickedListener() {
            @Override
            public void onItemClicked(ListContainer listContainer, Component component, int i, long l) {
                new ToastDialog(getContext()).setText("position:" + i).show();
            }
        });

    }

    /**
     * @since 2021-04-20
     */
    private class GridViewAdapter extends BaseItemProvider {
        @Override
        public int getCount() {
            return 31;
        }

        @Override
        public Object getItem(int i) {
            return null;
        }

        @Override
        public long getItemId(int i) {
            return 0;
        }

        @Override
        public Component getComponent(int i, Component view, ComponentContainer componentContainer) {
            if (view == null) {
                view = LayoutScatter.getInstance(getContext()).
                        parse(ResourceTable.Layout_sample_item_nested, componentContainer, false);
            }
            Image image = (Image) view.findComponentById(ResourceTable.Id_img_item_ns_model);
            if (image != null) {
                image.setPixelMap(mDogs[i % mDogs.length]);
            }
            return view;
        }
    }
}
